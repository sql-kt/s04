INSERT INTO artists (name) VALUES ("BLACKPINK");
INSERT INTO artists (name) VALUES ("TWICE");
INSERT INTO artists (name) VALUES ("Taylor Swift");

SELECT name FROM artists;
SELECT * FROM artists;

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("The Album", "2020-10-2", 1);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fancy You", "2019-10-2", 2);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red (Taylor's Version)", "2021-10-2", 3);


SELECT * FROM albums;

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Fancy", 333, "K-Pop", 2), ("How You Like That", 301, "K-Pop", 1), ("All Too Well (10 mins version", 1000, "Pop", 3);

SELECT * FROM songs;

SELECT song_name, genre FROM songs;
 SELECT song_name FROM songs WHERE genre = "K-pop";
SELECT song_name FROM songs WHERE genre = "k-pop";
 SELECT song_name FROM songs WHERE length >= 1000;
SELECT song_name FROM songs WHERE length >= 1000 AND genre = "pop";

VARCHAR standard for SQL
_ and %



JOINING TABLES
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;

SELECT * FROM artists
		JOIN albums ON artists.id = albums.artist_id
      		JOIN songs ON albums.id = songs.album_id;




SELECT name, album_title, date_released, song_name, length, genre FROM artists
		JOIN albums ON artists.id = albums.artist_id
        	JOIN songs ON albums.id = songs.album_id;

SELECT artists.name, albums.album_title, date_released, songs.song_name, length, genre FROM artists JOIN albums ON artists.id = albums.artist_id JOIN songs ON albums.id = songs.album_id;

SELECT name AS band, album_title AS album, date_released, song_name AS song, length, genre FROM artists JOIN albums ON artists.id = albums.artist_id JOIN songs ON albums.id = songs.album_id;

INSERT INTO users (username, password, full_name, contact_number, email, address) VALUES ("john", "john1234", "John Doe", 9123456789, "john@mail.com", "New York");

INSERT INTO playlists (user_id, datetime_created) VALUES (1, "2022-12-22 09:53:00");

INSERT INTO playlists_songs (playlist_id, song_id) VALUES (1, 1), (1,10),(1, 11);

SELECT full_name, datetime_created, song_name, length, genre, album_title FROM users
		JOIN playlists ON users.id = playlists.user_id
   	 	JOIN playlists_songs ON playlists.id = playlists_songs.playlist_id
   	 	JOIN songs ON playlists_songs.song_id = songs.id
    		JOIN albums ON songs.album_id = albums.id;



